#!/usr/bin/env python3
import angr
import sys

def main(argv):
    path_to_binary = argv[1]
    project = angr.Project(path_to_binary, auto_load_libs=False)
    initial_state = project.factory.entry_state()
    simulation = project.factory.simgr(initial_state)
    
    # check address and write here
    # ----------------------------------------
    avoid = 0x?????
    find = 0x?????
    # ----------------------------------------

    simulation.explore(find=find, avoid=avoid)

    if simulation.found:
        solution_state = simulation.found[0]
        solution = solution_state.posix.dumps(sys.stdin.fileno())
        print("[+] Success! Solution is: {}".format(solution.decode("utf-8")))
    else:
        raise Exception('Could not find the solution')


if __name__ == '__main__':
    main(sys.argv)
