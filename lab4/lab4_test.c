#include <stdio.h>
int find(int num1,int num2){
	while(num1!=0&&num2!=0){
		if(num1>num2)
			num1%=num2;
		else
			num2%=num1;
	}
	return num1+num2;
}
int main(void){
	int num1 = 0;
	int num2 = 0;
	int result = 0;
	scanf("%d %d",&num1,&num2);
	result = find(num1,num2);
	printf("%d\n",result);
	return 0;
}
