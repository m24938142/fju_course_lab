#!/bin/bash
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install default-jre default-jdk unzip -y
sudo apt-get install lcov -y
sudo apt-get install -y build-essential python3-dev automake git flex bison libglib2.0-dev libpixman-1-dev python3-setuptools
sudo apt-get install -y lld-11 llvm-11 llvm-11-dev clang-11 || sudo apt-get install -y lld llvm llvm-dev clang
sudo apt-get install -y gcc-$(gcc --version|head -n1|sed 's/.* //'|sed 's/\..*//')-plugin-dev libstdc++-$(gcc --version|head -n1|sed 's/.* //'|sed 's/\..*//')-dev

cp -r /home/ubuntu/images ./lab6
cp -r /home/ubuntu/buildroot ./tool/

wget https://dl.xpdfreader.com/old/xpdf-3.02.tar.gz -P ./lab8
cd lab8
tar -xvzf xpdf-3.02.tar.gz
rm -rf xpdf-3.02.tar.gz
cd ../
cp -r /home/ubuntu/AFLplusplus ./tool
cd tool/AFLplusplus
export LLVM_CONFIG="llvm-config-11"
make all
sudo make install
cd ../../

