# Introduction to Information Security
[TOC]
## Speaker
- Cheng-Yen Chung
    - National Taiwan University of Science and Technology
    - Department of Computer Science and Information Engineering
    - Mail : M10915029@mail.ntust.edu.tw
## Course Slide
- 6/10
    - System Security 1.pdf
- 6/17
    - System Security 2.pdf
## Lab 6/10
### Lab 1 GCC Operations
```
$ cd lab1
$ gcc -E lab1.c -o lab1.i
$ gcc -S lab1.c -o lab1.s
$ gcc -c lab1.s -o lab1.o
$ gcc lab1.o -o lab1
```
### Lab 2 Hook
* Compile and run
```
$ cd lab2
$ gcc -shared -fPIC -o lab2_share.so lab2_share.c
$ gcc -o lab2_test lab2_test.c
$ ./lab2_test
$ export LD_PRELOAD=./lab2_share.so
$ ./lab2_test
```
* Clean environment variable
```
$ export LD_PRELOAD=
```
### Lab 3 Cross Compile
```
$ cd lab3
$ arm-linux-gnueabihf-gcc -static -o lab3 lab3.c
$ qemu-arm-static ./lab3
```
### Lab 4 Ghidra
```
$ cd tool
$ unzip ghidra_10.0.2_PUBLIC_20210804.zip
$ cd ghidra_10.0.2_PUBLIC
$ ./ghidraRun
```
### Lab 5 Build Service
```
$ make
$ ./webserver
```

## Lab 6/17
### Installation
```
$ ./install.sh     # Password : 610617
```
### Lab 6 Buildroot
* Configure
```
$ cd tool/buildroot
$ qemu_arm_vexpress_defconfig
$ make                           # Takes a very long time
```
* Run
```
$ cd lab6
$ ./start-qemu.sh
```
### Lab 7 Code Coverage
* Compile
```
$ cd lab7
$ gcc -fprofile-arcs -ftest-coverage lab7.c -o lab7
```
* Run
```
$ ./lab7
```
* Generate analysis reports
```
$ gcov lab7.c
$ lcov -c -o lab7.info -d ./
$ genhtml lab7.info -o ./output
```
### Lab 8 AFL++ Fuzzing
* Compile xpdf
```
$ cd lab8/xpdf-3.02
$ CC=afl-clang-fast CXX=afl-clang-fast++ ./configure
$ make
$ sudo make install
```
* Setting up
```
$ sudo su
$ echo core >/proc/sys/kernel/core_pattern
$ exit
```
* Fuzzing
```
$ cd lab8
$ afl-fuzz -i ./pdf -o ./out -s 123 -- pdftotext @@
```
### Lab 9 Symbolic Execution
* Reverse Program
    * Ghidra
* Check address
    * Find
    * Avoid
* Modify lab9/symbolic.py
* Run symbolic Execution
```
$ cd lab9
$ ./symbolic.py lab9
```
